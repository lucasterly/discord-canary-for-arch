# Discord Canary for Arch

-------------------
Author : Lucasterly

GitLab : https://gitlab.com/lucasterly/discord-canary-for-arch


Discord canary version for linux :D

Instalation
--------
git clone https://gitlab.com/lucasterly/discord-canary-for-arch.git

cd discord-canary-for-arch

sudo pacman -U discord-canary-0.0.119-1-x86_64.pkg.tar.gz

------------
and enjoy!
